/**
 * Bootstrap
 *
 * An asynchronous boostrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#documentation
 */

module.exports.bootstrap = function (cb) {


	RoomService.createRoom('Lobby');
	// TODO: Upgrade to sails 0.9.7 and put this code in the onDisconnect event handler
	// Adding this here for a disconnect event handler
	sails.io.sockets.on('connection', function (socket) {
	 
	  socket.on('disconnect', function () {
	    UserService.removeUser(socket.id);
	  });

	});
  // It's very important to trigger this callack method when you are finished 
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};