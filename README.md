# AnonymousChatApplication

This is just a quick project I put together to demonstrate my coding style and ability to define a sensible Web Application architecture.

Some of the technologies used are AngularJS, Node.js, sails.js (which itself supplies Socket.IO, Express, Winston), Jade, Stylus (CSS Preprocessor).

Note that this project is not 100% complete, and is still a WIP. 

--

Installation Steps

1) Clone the Repository
2) Enter the local code repo with the terminal and type "npm install"
3) Enter "sails lift app.js" in the command prompt. 

Note: "node app.js" will not work since this is a sails.js project.