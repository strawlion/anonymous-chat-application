/**
 * MainController
 *
 * @module		:: Controller
 * @description	:: Contains logic for WebSocket requests
 */

// TODO: order these the same as request service
// TODO: Add server validation for all actions to protect against malicious input
module.exports = {

  addUser: function (request, response) {

    console.log(request)

    var newUserName = request.param('userName');

    UserService.addUser(newUserName, request.socket.id, function(error, wasUserAddedSuccessfully, newUserName) {
      if (error) throw error;


      if (wasUserAddedSuccessfully) {

        // Send the response
        var responseParameters = {};
        responseParameters.newUserName = newUserName;

        sails.io.sockets.emit('newUser', responseParameters);
        // response.broadcast('newUser', data);  // TODO: Use response broadcast after upgrading to sails 0.9.7responseParameters);
      }
    
    });

  },



  sendMessage: function (request, response) {

    var callerSocketID     = request.socket.id;
    var userSendingMessage = UserService.getUser(callerSocketID);

    var responseParameters = {};
    var messageToSend = userSendingMessage.name + ': ' + request.param('message');
  	responseParameters.message = messageToSend

  	sails.io.sockets.emit('newMessage', responseParameters);
  	// response.broadcast('newMessage', data);  // TODO: Use response broadcast after upgrading to sails 0.9.7
  },



  switchRoom: function(request, response) {

    var callerSocketID = request.socket.id;
    var roomToJoinName = request.param('roomToJoinName');

    UserService.switchRoom(roomToJoinName, callerSocketID, function(error, wasSuccessful) {

      
      if (wasSuccessful) {

        var requestParameters = {};
        requestParameters.wasSuccessful;

        response.json(requestParameters);
      }

    });

  },


  getRoomNames: function(request, response) {

    var responseParameters = {};
    responseParameters.newRoomNames = RoomService.getRoomNames();

    response.json(responseParameters);
  },


  createRoom: function(request, response) {
    

    var newRoomName = request.param('newRoomName');
    sails.log('Attempting to create room: ' + newRoomName);

    RoomService.createRoom(newRoomName, function(error, wasRoomAddedSuccessfully) {


      if (!wasRoomAddedSuccessfully) {
        return;
      }

      var responseParameters = {};
      responseParameters.newRoomName = newRoomName;
      sails.io.sockets.emit('newRoom', responseParameters);
      response.send();
    });

    // response.broadcast('newRoom', data);  // TODO: Use response broadcast after upgrading to sails 0.9.7
  }


};
