// UserService - Handles all interactions with the users object without directly exposing it.

var users = {};

// Adds a user to the users object. Each username must be unique, and if the user declines to
// enter a username he will be assigned a random username
exports.addUser = function(newUserName, socketID, callback) {

	sails.log('Adding username: ' + newUserName + ' for socket: ' + socketID);
	var wasUserSuccessfullyAdded = null;


	// If the user declined to select a username, give them a random one
	if (newUserName === null) {
		
		var newAnonymousUserName = generateUniqueAnonymousUserName();
		var newUser              = createUser(newAnonymousUserName);


		users[socketID] = newUser;
		wasUserSuccessfullyAdded = true;
	}
	// Else if the user has decided to select a username
	else {

		var isUserNameUnique = validateUniqueUserName(newUserName);
		if (isUserNameUnique) {

			var newUser = createUser(newUserName);

			users[socketID] = newUser;
			wasUserSuccessfullyAdded = true;
		}
		// Else if the username already exists
		else {
			wasUserSuccessfullyAdded = false;
		}

	}


	if (callback) {
		var newUserNameToSendBack = users[socketID].name;
		callback(null, wasUserSuccessfullyAdded, newUserNameToSendBack); // error, wasUserSuccessfullyAdded, newUserNameToSendBack
	}

};


exports.switchRoom = function(roomToJoinName, socketID, callback) {
	
	var currentUser = users[socketID];
	var roomUserIsLeavingName = currentUser.room;
	var roomUserIsLeaving = RoomService.getRoom(currentUser.room);
	var roomUserIsJoining = RoomService.getRoom(roomToJoinName);

	// If the user is trying to join the room he is already in
	if (roomUserIsLeaving === roomUserIsJoining) {
		
		if (callback) {
			callback(null, false); // error, wasSuccessful
			return;
		}
	}

	currentUser.room = roomToJoinName;             // Replace this user's room with the new room
	roomUserIsJoining.numberOfUsers++;
	roomUserIsLeaving.numberOfUsers--;


	if (roomUserIsLeaving.numberOfUsers === 0 && roomUserIsLeavingName !== 'Lobby') {
		RoomService.removeRoom(roomUserIsLeavingName);
	}

		if (callback) {
			callback(null, true); // error, wasSuccessful
			return;
		}
};

exports.removeUser = function(socketID) {
	
	var userToRemove = users[socketID];
	var roomUserIsIn = RoomService.getRoom(userToRemove.room);
	roomUserIsIn.numberOfUsers--;
	
	if (roomUserIsIn.numberOfUsers === 0) {
		RoomService.removeRoom(userToRemove.room);
	}

	delete users[socketID];
};

exports.getUser = function(socketID) {
	return users[socketID];
}

exports.getUsers = function() {
	return users;
};




/* Helper Methods */

// Creates the user object to add to the user list.
// All users default to the lobby for now.
function createUser(userName) {
	var newUser = {};
	newUser.name = userName;
	newUser.room = 'Lobby';

	var lobby = RoomService.getRoom(newUser.room);
	lobby.numberOfUsers++;

	return newUser;
}

// Validate that the supplied username is unique
function validateUniqueUserName(newUserName) {
	
	// Validate that the new username is unique
	for (currentSocketID in users) {
		var currentUser = users[currentSocketID];

		if (currentUser.name === newUserName) {
			return false;
		}

	}

	return true;

}

// Creates a an anonymous username using a random string
function generateUniqueAnonymousUserName() {

	var randomString = generateRandomString();
	var anonymousUserName = 'anonymous' + randomString;
	var isUniqueUserName = validateUniqueUserName(anonymousUserName);		

	while (!isUniqueUserName) {
	
		randomString = generateRandomString();
		anonymousUserName = 'anonymous' + randomString;
		isUniqueUserName = validateUniqueUserName(anonymousUserName);		
	}

	return anonymousUserName;
}

// Generates a pseudo-random string
function generateRandomString() {
	return Math.random().toString(36).slice(2);
}