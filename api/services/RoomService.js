// RoomService - Handles all interactions with the rooms object without directly exposing it.

var defaultRoom = createRoom();
var rooms = {
	'Lobby': defaultRoom
};

// Adds a room to the room object if it has a unique name, otherwise don't
exports.createRoom = function(roomName, callback) {
	var isUniqueRoomName = validateUniqueRoomName(roomName);
	var wasRoomAddedSuccessfully = false;

	if (isUniqueRoomName) {

		rooms[roomName] = createRoom();
		sails.log('Rooms: ' + JSON.stringify(rooms));
		wasRoomAddedSuccessfully = true;
	}

	if (callback) {

		callback(null, wasRoomAddedSuccessfully); // error, wasRoomAddedSuccessfully
	}

};


exports.removeRoom = function(roomToRemoveName) {
	delete rooms[roomToRemoveName];

	var responseParameters = {};
	responseParameters.removedRoomName = roomToRemoveName;
	sails.io.sockets.emit('roomRemoved', responseParameters); // TODO: Move socket events into a service
	
};

exports.getRoom = function(roomName) {
	return rooms[roomName];
};
exports.getRooms = function() {
	return rooms;
};

exports.getRoomNames = function() {

	var roomNames = [];

	for (roomName in rooms) {
		roomNames.push(roomName);
	}

	return roomNames;
};



/* Helper Methods */

// Validate that the supplied username is unique
function validateUniqueRoomName(roomName) {
	sails.log('Validating that ' + roomName + ' is unique;')

	var isUniqueRoomName = (rooms[roomName] === undefined);
	return isUniqueRoomName;
}


// Creates the room object to add to the room list
function createRoom(roomName) {
	var newRoom = {};
	newRoom.numberOfUsers = 0;

	return newRoom;
}