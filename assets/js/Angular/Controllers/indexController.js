// indexController - The index controller controls the home page (and currently the only page), behavior.
//                   It contains models, event listeners, and click handlers.
app.controller('indexController', function ($scope, eventService, requestService, promptService) {

    // Models
    $scope.userName         = '';
    $scope.conversationText = '';
    $scope.messageToSend    = '';

    $scope.roomNames        = [];
    $scope.selectedRoom     = '';

    var messages = [];


    // Sends the chat message to the server which will then send it to all connected users
    $scope.sendMessage = function() {
    
        requestService.sendMessage($scope.messageToSend);
        $scope.messageToSend = '';
    };


    $scope.selectRoom = function(roomName) {
        $scope.selectedRoom = roomName;
    };

    $scope.switchRoom = function(roomName) {

        var userSwitchingRooms = $scope.userName;
        requestService.switchRoom(roomName, function(wasSuccessful) {

            if (!wasSuccessful) {
                return;
            }

            displayNewChatMessage('You have entered: ' + roomName);
            $scope.$apply();
        });
    };

    // Create a new chat room
    $scope.createRoom = function() {
        
        promptService.prompt('What is the room name?', function(resultName) {
            if (resultName === null) return;

            requestService.createRoom(resultName, function() {

                $scope.switchRoom(resultName);
            });

        });

    };


    // Handle key events when focused on the message input box
    $scope.onChatInputKeyPress = function(event) {

        // Enter Key
        if (event.keyCode === 13) {
            $scope.sendMessage();
        }
    }



    /* Event Handlers */

    // Fires whenever a new user joins the chat
    function userAddedListener(newUserName) {

      displayNewChatMessage(newUserName + ' has entered the chat room.');
      $scope.$apply(); // let angular know to refresh the view
    }

    // Fires whenever a connected user adds a new room
    function roomAddedListener(newRoomName) {

        $scope.roomNames.push(newRoomName);
        displayNewChatMessage('A new room has been created: ' + newRoomName);
        $scope.$apply(); // Let angular know to refresh the view
    }

    // Fires whenever a room has been removed
    function roomRemovedListener(removedRoomName) {

        for(var i = 0; i < $scope.roomNames.length; i++) {
            var currentRoomName = $scope.roomNames[i];

            if (currentRoomName === removedRoomName) {
                $scope.roomNames.splice(i, 1);
            }
        }

        $scope.$apply();
    }


    // Fires whenever a connected user sends a message
    function messageReceivedListener(message) {


        // TODO: Right now we are storing messages in an array and also in conversationText.
        // Fix this by using angular parser to make array the model and custom determine the output
        messages.push(message);
        displayNewChatMessage(message);

        // Scroll to the end of the chat window
        $("#messagePool").scrollTop(99999) // TODO: move DOM manipulation out of the controller

        $scope.$apply(); // Let angular know to refresh the view
    }



    /* Local Functions */

    // Asks the user for their desired username. If they do not supply one, it will randomly generate one for them.
    function promptForUserName() {

            promptService.prompt('What is your name?', function(resultName) {

                requestService.addUser(resultName, function(data) {
                    var wasUserAddedSuccessfully = data.wasUserAddedSuccessfully;
                    var newUserName = data.newUserName;

                    // If the user wasn't added successfully, tell the user that 
                    if (!wasUserAddedSuccessfully) {
                        promptService.alert('That name has already been taken!', function(result) {
                        promptForUserName();
                    });

                    }
                    else {
                        $scope.user.name = newUserName;
                    }

                }); // End addUser

            }); // End prompt
    }

    function displayNewChatMessage(newMessage) {
        $scope.conversationText = $scope.conversationText + newMessage + '\n';
    }


    // Actions to perform on page initialization
    function init() {

        requestService.getRoomNames(function(data) {
            
            $scope.roomNames = data.newRoomNames;
            $scope.$apply();
        });

        eventService.addMessageReceivedListener(messageReceivedListener);
        eventService.addRoomAddedListener(roomAddedListener);
        eventService.addRoomRemovedListener(roomRemovedListener);
        eventService.addUserAddedListener(userAddedListener);

        promptForUserName();
    }

    // Initalize the page
    init();
});