// requestService - Provides a way for controllers and other services to make requests to the server. 
//                  By placing these events in a service we can abstract away the act of making requests
//                  to the callers by instead providing them a simple API
app.service('requestService', function() {


    var serviceObject = {

        addUser: function(userName, callback) {

            var requestData = {};
            requestData.userName = userName;

            socket.post('/main/addUser', 
                        { userName: userName }, 
                        function(response) {
                        
                            if (callback) {
                                callback(response);
                            }

                    });

        },

        sendMessage: function(message, callback) {
            
            socket.post('/main/sendMessage', 
                        { message: message }, 
                        function(response) {
                        
                            if (callback) {
                                callback(response);
                            }

                    });
        
        },

        switchRoom: function(roomToJoinName, callback) {

            socket.post('/main/switchRoom',
                        { roomToJoinName: roomToJoinName },
                        function(response) {

                            if (callback) {
                                callback(response);
                            }

                        });

        },

        createRoom: function(newRoomName, callback) {

            socket.post('/main/createRoom',
                        { newRoomName: newRoomName },
                        function(response) {

                            if (callback) {
                                callback(response);
                            }

                        });

        },

        getRoomNames: function(callback) {
            
            socket.post('/main/getRoomNames',
                        { },
                        function(response) {

                            if (callback) {
                                callback(response);
                            }

                        });
        }

    }

    return serviceObject;

});