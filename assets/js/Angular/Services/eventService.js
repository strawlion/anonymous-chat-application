// eventService - Allows controllers to subscribe to any events pertinent to their operation. As of right now
//                we only have one controller, but if we were to have multiple controllers they could all receive
//                events without having multiple socket.on calls in each controller. The listeners arrays contain
//                function pointers to the subscriber's "onEvent" listener which is called when the event takes place

app.service('eventService', function() {

    var messageReceivedListeners = [];
    var roomAddedListeners       = [];
    var roomRemovedListeners     = [];
    var userAddedListeners       = [];



    // Call all message received listeners with the data sent with the message
    socket.on('newMessage', function(data) {

        var message = data.message;

        for (var i = 0; i < messageReceivedListeners.length; i++) {
            var currentMessageReceivedListener = messageReceivedListeners[i];
            currentMessageReceivedListener(message);
        }

    });


    // Call all room added listeners with the data sent with the message
    socket.on('newRoom', function(data) {

        var newRoomName = data.newRoomName;

        for (var i = 0; i < roomAddedListeners.length; i++) {
            var currentRoomAddedListener = roomAddedListeners[i];
            currentRoomAddedListener(newRoomName);
        }

    });

    // Call all room removed listeners with the data sent with the message
    socket.on('roomRemoved', function(data) {

        var removedRoomName = data.removedRoomName;

        for (var i = 0; i < roomRemovedListeners.length; i++) {
            var currentRoomRemovedListener = roomRemovedListeners[i];
            currentRoomRemovedListener(removedRoomName);
        }

    });

    // Call all user added listeners with the data sent with the message
    socket.on('newUser', function(data) {

        var newUserName = data.newUserName;

        for (var i = 0; i < userAddedListeners.length; i++) {
            var currentUserAddedListener = userAddedListeners[i];
            currentUserAddedListener(newUserName);
        }

    });

    var serviceObject = {
        
        // Accepts a function pointer to a callback function that will fire 
        // whenever a chat message is received
        addMessageReceivedListener: function(messageReceivedListener) {
           messageReceivedListeners.push(messageReceivedListener);
        },

        addRoomAddedListener: function(roomAddedListener) {
            roomAddedListeners.push(roomAddedListener);
        },

        addRoomRemovedListener: function(roomRemovedListener) {
            roomRemovedListeners.push(roomRemovedListener);
        },

        addUserAddedListener: function(userAddedListener) {
            userAddedListeners.push(userAddedListener);
        }

        

    }

    return serviceObject;

});