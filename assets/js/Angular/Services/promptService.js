// This service uses the bootbox.js library to create Bootstrap
// modals (popups) that are easy to use
app.service('promptService', function() {


    // TODO: just pass callback object?
    var serviceObject = {
        
        prompt: function(message, callback) {
           
            bootbox.prompt(message, function(result) {
                
                if (callback) {
                    callback(result);
                }

            });
        },

        alert: function(message, callback) {

            bootbox.alert(message, function(result) {
                
                if (callback) {
                    callback(result);
                }

            });

        },

        confirm: function(message, callback) {

            bootbox.confirm(message, function(result) {
                
                if (callback) {
                    callback(result);
                }

            });
        }

    }

    return serviceObject;

});